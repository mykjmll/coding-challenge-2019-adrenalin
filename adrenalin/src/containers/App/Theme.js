const Theme = {
  fontSize: {
    sm: '7px',
    md: '13px',
    lg: '21px',
    xl: '24px',
    xxl: '32px',
    xxxl: '64px',
  },
  mediaQuery: {
    min: {
      xs: 'min-width: 576px',
      sm: 'min-width: 768px',
      md: 'min-width: 992px',
      lg: 'min-width: 1200px',
      xl: 'min-width: 1366px',
    },
    max: {
      xs: 'max-width: 575.98px',
      sm: 'max-width: 767.98px',
      md: 'max-width: 991.98px',
      lg: 'max-width: 1199.98px',
      xl: 'max-width: 1365.98px',
      xxl: 'max-width: 1399.98px',
    },
  },
};

export default Theme;
