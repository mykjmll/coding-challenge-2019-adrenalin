import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import Layout from '../../components/Layout';
import ListPage from '../ListPage';
import DetailPage from '../DetailPage';

// default themes
import theme from './Theme';

function App() {
  const ListPageRedirect = () => <Redirect to="/list" />;
  return (
    <ThemeProvider theme={theme}>
      <React.Fragment>
        <Layout>
          <Switch>
            <Route exact path="/" component={ListPageRedirect} />
            <Route exact path="/list" component={ListPage} />
            <Route path="/list/detail/:id" component={DetailPage} />
          </Switch>
        </Layout>
      </React.Fragment>
    </ThemeProvider>
  );
}

export default App;