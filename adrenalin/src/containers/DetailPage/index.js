/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect} from 'react';
import {compose} from 'redux';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import axios from 'axios';
import {setData} from './action';
import {withTheme} from 'styled-components';
import StyledDetailTemplate from './StyledDetailTemplate';
import Image from '../../components/Image';
import {localUrl} from '../../utils/constants';

const DetailPage = ({match, theme, state, onSetFeed}) => {
  const id = match.params.id;
  useEffect(() => {
    // will get data of a specific id then will set on global state
    axios.get(`${localUrl}/data/data.json`)
    .then(response => { 
      const feed = response && response.data.find(f => f.id === Number(id) )
      onSetFeed('feed', feed);
    })
  }, []);
  const {feed} = state;
  return(
    <React.Fragment>
      {feed && (
      <StyledDetailTemplate className="row">
        <div className="col-12 col-lg-6 pr-lg-5 pr-3">
          <Image src={`${localUrl}/assets/${feed.image}`} alt="img-detail" tag={feed.tag} />
        </div>
        <div className="col-12 col-lg-6 pl-lg-5 pl-3">
          <div className="title">
            <span>{feed.title}</span>
          </div>
          <div>
          {feed.questions && feed.questions.map((ques, idx) => (
            <div className="question" key={idx + 1}>
              <span className="questionNo">{`Question ${idx + 1}`}</span>
              <span>{ques}</span>
            </div>
          ))}
          </div>
        </div>
      </StyledDetailTemplate>
      )}
    </React.Fragment>
  );
}

DetailPage.propTypes = {
  match: PropTypes.object, 
  theme: PropTypes.object, 
  state: PropTypes.any, 
  onSetFeed: PropTypes.func
}

const mapStateToProps = state => {
  return {
    state: state.DetailPageReducer,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSetFeed: (key, data) => dispatch(setData(key, data))
  }
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withTheme
)(DetailPage);
