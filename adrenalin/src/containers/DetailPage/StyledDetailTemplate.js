import styled from 'styled-components';

const StyledDetailTemplate = styled.div`
  padding-bottom: 15px;

  .title {
    width: 100%;
    margin: 25px 0;
    font-size: ${props => props.theme.fontSize.xxl};
    font-weight: bold;
  }

  .question {
    margin-bottom: 1rem;
    span {
      display: block;
      font-size: ${props => props.theme.fontSize.md};
    }
    span.questionNo {
      font-size: ${props => props.theme.fontSize.xl};
      font-weight: bold;
    }
  }

  @media (${props => props.theme.mediaQuery.min.lg}) {
    padding-bottom: 30px;

    .title {
      width: 80%;
      margin: 50px 0;
      font-size: ${props => props.theme.fontSize.xxxl};
    }
  
    .question {
      margin-bottom: 0.5rem;
      span {
        font-size: ${props => props.theme.fontSize.lg};
      }
      span.questionNo {
        font-size: ${props => props.theme.fontSize.xxl};
      }
    }
  }
` 
export default StyledDetailTemplate