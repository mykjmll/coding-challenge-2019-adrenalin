export const initialState = {
  feed: false,
}

const DetailPageReducer = (state = initialState, action) => { 
  switch (action.type) {
    case 'SET_DATA':
      const key = action.key;
      return {...state, [key]: action.data}
    default:
      return state
  }
}

export default DetailPageReducer;