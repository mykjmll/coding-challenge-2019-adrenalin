import styled from 'styled-components';

const StyledListTemplate = styled.div`
  margin-bottom: 25px;
  .image-container {
    margin-bottom: 15px;
  }
  .title-container {
    text-transform: capitalize;
    font-weight: bold;
    font-size: ${props => props.theme.fontSize.xl};
    margin-bottom: 5px;
    width: 100%;
    word-break: break-word;
  }
  .link-container {
    span {
      width: 30px;
      border-top: 2px solid #3852f7;
      height: 9px;
      display: inline-block;
      margin-right: 15px;
    }
    text-transform: uppercase;
    font-size: ${props => props.theme.fontSize.md};
    color: #3852f7;
    font-weight: bold;

    :hover {
      cursor: pointer;
      span {
        border-top: none;
        border-bottom: 2px solid #3852f7;
      }
    }
  }

  @media (${props => props.theme.mediaQuery.min.lg}) {
    margin-bottom: 50px;
    .image-container {
    }
  .title-container {
    width: 70%;
  }
  }
` 
export default StyledListTemplate