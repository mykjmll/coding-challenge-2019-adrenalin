/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {compose} from 'redux';
import PropTypes from 'prop-types';
import axios from 'axios';
import {setData} from './action';
import StyledListTemplate from './StyledListTemplate';
import { forwardTo } from '../../utils/commonHelper';
import Image from '../../components/Image';
import {localUrl} from '../../utils/constants';

const ListPage = ({state, onSetList}) => {
  useEffect(() => {
    // will get list then will set on global state
    axios.get(`${localUrl}/data/data.json`)
    .then(response => { 
      onSetList('feedList', response.data);
    })
  }, []);
  const {feedList} = state;
  return(
    <React.Fragment>
      <div className="row">
        {feedList && feedList.map(data => (
          <StyledListTemplate className="col-12 col-lg-6" key={data.id}>
          <div className="image-container">
            <Image src={`${localUrl}/assets/${data.thumb}`} alt="list-img" tag={data.tag} />
          </div>
          <div className="title-container"> 
            {data.title_long}
          </div>
          <div className="link-container" onClick={() => forwardTo(`/list/detail/${data.id}`)}>
            <span></span>
            view case study
          </div>
        </StyledListTemplate>
        ))}
      </div>
    </React.Fragment>
  );
}

ListPage.propTypes = {
  state: PropTypes.any, 
  onSetList: PropTypes.func
}

const mapStateToProps = state => {
  return {
    state: state.ListPageReducer,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSetList: (key, data) => dispatch(setData(key, data))
  }
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
)(ListPage);