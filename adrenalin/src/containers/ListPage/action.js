export function setData(key, data) {
  return {
    type: 'SET_DATA',
    key,
    data
  };
}