import {combineReducers} from 'redux';
import { connectRouter } from 'connected-react-router';

import history from './utils/history';
import DetailPageReducer from './containers/DetailPage/reducer';
import ListPageReducer from './containers/ListPage/reducer';

const rootReducer = combineReducers({
  router: connectRouter(history),
  ListPageReducer,
  DetailPageReducer,
})

export default rootReducer;