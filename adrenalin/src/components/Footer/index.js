import React from 'react';
import PropTypes from 'prop-types';
import {NavLink, Link} from 'react-router-dom';
import StyledFooter from './StyledFooter';
import { ReactComponent as Logo } from '../../assets/adrenalin.svg';

const Footer = ({className}) => {
  return (
    <StyledFooter className={className}>
      <div>
        <div className="logo">
          <Link to="/"><Logo /></Link>
        </div>
        <ul>
          <li><NavLink to="/">Privacy</NavLink></li>
          <li><NavLink to="/">Sitemap</NavLink></li>
          <li><NavLink to="/">Facebook</NavLink></li>
          <li><NavLink to="/">LinkedIn</NavLink></li>
          <li><NavLink to="/">Instagram</NavLink></li>
          <li><NavLink to="/">Twitter</NavLink></li>
        </ul>
      </div>
    </StyledFooter>
  );
}

Footer.propTypes = {
  className: PropTypes.string,    
}

export default Footer