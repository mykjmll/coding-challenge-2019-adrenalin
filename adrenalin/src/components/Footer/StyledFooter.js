import styled from 'styled-components';

const StyledFooter = styled.div` 
  margin-top:auto;
  > div {
    border-top: 2px solid #000000;
    padding-top: 25px;
  }
  .logo {
    display: none;
  }
  ul {
    list-style-type: none;
    padding-inline-start: 0;
    margin-bottom: 0;
    background-color: transparent;
    li {
      text-align: center;
      margin: 0 auto;
      a {
        color: #000000; 
        :hover {
          cursor: pointer;
          text-decoration: none;
        }
      }
    }
  }

  @media (${props => props.theme.mediaQuery.min.lg}) {
    > div {
      display: flex;
      justify-content: space-between;
    }
    .logo {
      display: block;
      svg {
        width: 100px;
        .st0 {
          fill: #000000;
        }
      }
    }
    ul {
      display: flex;
      flex-direction: row;
      justify-content: flex-end;
      li {
        a {
          margin-left: 40px;
        }
      }
    }
  }
`
export default StyledFooter