import React from 'react';
import PropTypes from 'prop-types';
import {NavLink, Link} from 'react-router-dom';
import StyledHeader from './StyledHeader';
import { ReactComponent as Logo } from '../../assets/adrenalin.svg';
import HamburgerMenu from '../../assets/navigation-menu.svg';

const Header = ({className}) => {
  const toggleActive = () => {
    const menu = document.getElementById("js-menu");
    menu.classList.toggle('active');
  }
  return (
    <StyledHeader className={`${className}`}>
      <span className="navbar-toggle" onClick={toggleActive}>
        <img src={HamburgerMenu} alt="HamburgerMenu" />
      </span>
      <div className="logo">
        <Link to="/"><Logo /></Link>
      </div>
      <ul className="main-nav" id="js-menu">
        <li><NavLink to="/" className="nav-links">Culture</NavLink></li>
        <li><NavLink to="/" className="nav-links">Work</NavLink></li>
        <li><NavLink to="/" className="nav-links">Clients</NavLink></li>
        <li><NavLink to="/" className="nav-links">Services</NavLink></li>
        <li><NavLink to="/" className="nav-links">Careers</NavLink></li>
        <li><NavLink to="/" className="nav-links">Contact</NavLink></li>
      </ul>
    </StyledHeader>
  );
}

Header.propTypes = {
  className: PropTypes.string,    
}

export default Header