import styled from 'styled-components';

const StyledHeader = styled.div`
position: relative;

.navbar-toggle {
  position: absolute;
  right: 0;
  height: 25px;
  width: 25px;
  cursor: pointer;
  z-index: 2;
}
.logo {
  svg {
    width: 100px;
    .st0 {
      fill: #000000;
    }
  }
}
.main-nav {
  &.active {
    max-height: 145px;
  }
  max-height: 0;
  overflow-y: hidden;
  -webkit-transition: all .5s cubic-bezier(0, 1, 0.5, 1);
  transition: all .5s cubic-bezier(0, 1, 0.5, 1);
  list-style-type: none;
  padding-inline-start: 0;
  margin-bottom: 0;
  background-color: #000000;
  position: absolute;
  z-index: 3;
  display: block;
  width: 35%;
  margin-top: 5px;
  right: 1px;
  li {
    text-align: center;
    margin: 0 auto;
    .nav-links {
      color: #ffffff;
      :hover {
        cursor: pointer;
        text-decoration: none;
      }
    }
  }
}

@media (${props => props.theme.mediaQuery.min.lg}) {
    display: flex;
    justify-content: space-between;
    align-items: center;

    .navbar-toggle {
      display: none;
    }
    .main-nav {
      max-height: 40px;
      display: flex;
      flex-direction: row;
      justify-content: flex-end;
      background-color: transparent;
      position: absolute;
      z-index: 3;
      width: auto;
      margin-top: 0;
      right: 0;
      li {
        margin: 0;
        .nav-links {
          color: #000000;
          margin-left: 40px;
        }
      }
    }
  }
`
export default StyledHeader