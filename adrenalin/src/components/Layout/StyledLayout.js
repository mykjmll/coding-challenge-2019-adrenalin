import styled from 'styled-components';

const StyledLayout = styled.div`
  padding: 20px 25px;
  display:flex; 
  flex-direction:column; 
  min-height: 100vh;

  .main-wrapper {
    padding: 30px 0;
  }

  @media (${props => props.theme.mediaQuery.min.lg}) {
    padding : 40px 95px;

    .main-wrapper {
      padding: 60px 0;
    }
  }
`
export default StyledLayout