import React, {Children} from 'react';
import StyledLayout from './StyledLayout';
import PropTypes from 'prop-types';
import Header from '../Header';
import Footer from '../Footer';

const Layout = ({className, children}) => {
  return (
    <StyledLayout className={className}>
      <Header />
        <div className="main-wrapper">
          {Children.toArray(children)}
        </div>
      <Footer />
    </StyledLayout>
  );
}

Layout.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
}

export default Layout