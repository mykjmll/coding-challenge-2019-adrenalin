import styled from 'styled-components';

export const StyledImage = styled.div`
  position: relative;

  span.item-2 {
    height: 90px;
    width: 25px;
    position: absolute;
    display: block;
    top: -1px;
    left: -1px;
    z-index: 2;
    background: #ffffff;
  }

  div {
    position: absolute;
    top: 120px;
    left: -18px;
    -webkit-transform: rotate(-90deg);
    -ms-transform: rotate(-90deg);
    transform: rotate(-90deg);
    -webkit-transform: rotate(-90deg);
    -moz-transform: rotate(-90deg);
    width: 105px;
    text-align: right;
    span {
      font-size: ${props => props.theme.fontSize.md};
      color: #6c6c6c;
      text-transform: uppercase;
    }
  }

  @media (${props => props.theme.mediaQuery.min.lg}) {
    div {
      top: 145px;
      left: -40px;  
      width: 160px;
      span {
        font-size: 20px;
      }
    }
  }

` 
export default StyledImage