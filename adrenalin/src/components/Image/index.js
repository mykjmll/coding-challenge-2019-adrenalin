import React from 'react';
import StyledImage from './StyledImage';
import PropTypes from 'prop-types';

const Image = ({className, src, alt, tag}) => {
  return (
    <StyledImage>
      <span className="item-2" />
  <div className="item-3"><span>{tag}</span></div>
      <img className={`${className} item-1`} src={src} alt={alt} />
    </StyledImage>
  );
}

Image.propTypes = {
  className: PropTypes.string,
  src: PropTypes.string,
  alt: PropTypes.string,
  tag: PropTypes.string,
}

export default Image